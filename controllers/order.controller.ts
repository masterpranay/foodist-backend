import { Request, Response } from "express";
import OrderService from "../services/order.service";

class OrderController {
  async createOrder(req: Request, res: Response) {
    try {
      const order = await OrderService.createOrder(req.body);
      res.status(201).json(order);
    } catch (err: any) {
      res.status(500).json({
        error: err.message,
      });
    }
  }

  async getOrdersIdOfUser(req: Request, res: Response) {
    try {
      const orders = await OrderService.getOrdersId(
        `user='${req.params.userId}'`
      );
      res.status(200).json(orders);
    } catch (err: any) {
      res.status(500).json({
        error: err.message,
      });
    }
  }

  async getOrdersIdOfRestaurant(req: Request, res: Response) {
    try {
      const orders = await OrderService.getOrdersId(
        `restaurant='${req.params.restaurantId}'`
      );
      res.status(200).json(orders);
    } catch (err: any) {
      res.status(500).json({
        error: err.message,
      });
    }
  }

  async getOrderById(req: Request, res: Response) {
    try {
      const order = await OrderService.getOrderById(req.params.orderId);
      res.status(200).json(order);
    } catch (err: any) {
      res.status(500).json({
        error: err.message,
      });
    }
  }

  async getLastOrders(req: Request, res: Response) {
    const limit = req.query.limit;
    const page = req.query.page;
    try {
      const orders = await OrderService.getLastOrders({
        limit: limit ? parseInt(limit as string) : 10,
        page: page ? parseInt(page as string) : 1,
      });
      res.status(200).json(orders);
    } catch (err: any) {
      res.status(500).json({
        error: err.message,
      });
    }
  }

  async acceptOrder(req: Request, res: Response) {
    try {
      const order = await OrderService.updateOrder({
        id: req.params.orderId,
        data: {
          status: "accepted",
        },
      });
      res.status(200).json(order);
    } catch (err: any) {
      res.status(500).json({
        error: err.message,
      });
    }
  }

  async rejectOrder(req: Request, res: Response) {
    try {
      const order = await OrderService.updateOrder({
        id: req.params.orderId,
        data: {
          status: "rejected",
        },
      });
      res.status(200).json(order);
    } catch (err: any) {
      res.status(500).json({
        error: err.message,
      });
    }
  }

  async dispatchOrder(req: Request, res: Response) {
    try {
      const order = await OrderService.updateOrder({
        id: req.params.orderId,
        data: {
          status: "dispatched",
        },
      });
      res.status(200).json(order);
    } catch (err: any) {
      res.status(500).json({
        error: err.message,
      });
    }
  }

  async deliverOrder(req: Request, res: Response) {
    try {
      const order = await OrderService.updateOrder({
        id: req.params.orderId,
        data: {
          status: "delivered",
        },
      });
      res.status(200).json(order);
    } catch (err: any) {
      res.status(500).json({
        error: err.message,
      });
    }
  }
}

export default new OrderController();
