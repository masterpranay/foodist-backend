import { Request, Response } from "express";
import locationService from "../services/location.service";
import userService from "../services/user.service";

interface Location {
  stateId: string;
  cityId: string;
  houseNumber: string;
  locality: string;
  pincode: string;
  isCurrentLocation: boolean;
  userId: string;
}

class LocationController {
  async getAllStates(req: Request, res: Response) {
    try {
      const states = await locationService.getAllStates();
      res.status(200).json(states);
    } catch (err: any) {
      res.status(500).json({
        error: err.message,
      });
    }
  }

  async getAllCities(req: Request, res: Response) {
    try {
      const cities = await locationService.getAllCities(req.params.stateId);
      res.status(200).json(cities);
    } catch (err: any) {
      res.status(500).json({
        error: err.message,
      });
    }
  }

  async addLocation(req: Request, res: Response) {
    try {
      const location = await locationService.addLocation({
        stateId: req.body.stateId,
        cityId: req.body.cityId,
        houseNumber: req.body.houseNumber,
        locality: req.body.locality,
        pincode: req.body.pincode,
        isCurrentLocation: req.body.isCurrentLocation,
        userId: req.body.userId,
      });

      const user = await userService.findUserById(req.body.userId);

      const userUpdated = await userService.updateUser(req.body.userId, {
        locationId: [...user.locationId, location.id],
      });

      res.status(200).json(location);
    } catch (err: any) {
      res.status(500).json({
        error: err.message,
      });
    }
  }

  async updateLocation(req: Request, res: Response) {
    try {
      const location = await locationService.updateLocation(
        {
          stateId: req.body.stateId,
          cityId: req.body.cityId,
          houseNumber: req.body.houseNumber,
          locality: req.body.locality,
          pincode: req.body.pincode,
          isCurrentLocation: req.body.isCurrentLocation,
          userId: req.body.userId,
        },
        req.params.locationId
      );

      res.status(200).json(location);
    } catch (err: any) {
      res.status(500).json({
        error: err.message,
      });
    }
  }

  async deleteLocation(req: Request, res: Response) {
    try {
      await locationService.deleteLocation(
        req.params.locationId
      );

      res.status(200).json({
        message: "Location deleted successfully",
      });
    } catch (err: any) {
      res.status(500).json({
        error: err.message,
      });
    }
  }

  async getAllLocations(req: Request, res: Response) {
    try {
      const locations = await locationService.getAllLocations(req.params.userId);
      res.status(200).json(locations);
    } catch (err: any) {
      res.status(500).json({
        error: err.message,
      });
    }
  }
}

export default new LocationController();
