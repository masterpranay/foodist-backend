import { Request, Response } from "express";
import DishService from "../services/dish.service";

class DishController {
  // @Get('/get-all-dishes-id/:restaurantId')
  async getAllDishesId(req: Request, res: Response) {
    const { restaurantId } = req.params;
    let name = null
    if (req.query.name) {
      name = req.query.name as string
    }
    try {
      const filter = name ? `name%7E%27${name}%27` : ''
      const dishes = await DishService.getAllDishesId(restaurantId, filter);
      res.status(200).json(dishes);
    } catch (error: any) {
      res.status(500).json({ error: error.message });
    }
  }

  // @Get('/get-dish-by-id/:id')
  async getDishById(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const dish = await DishService.getDishById(id);
      res.status(200).json(dish);
    } catch (error: any) {
      res.status(500).json({ error: error.message });
    }
  }

  // @Post('/create-dish')
  async createDish(req: Request, res: Response) {
    try {
      const dish = await DishService.createDish(req.body);
      res.status(201).json(dish);
    } catch (error: any) {
      res.status(500).json({ error: error.message });
    }
  }

  // @Patch('/update-dish/:id')
  async updateDish(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const dish = await DishService.updateDish(id, req.body);
      res.status(200).json(dish);
    } catch (error: any) {
      res.status(500).json({ error: error.message });
    }
  }

  // @Delete('/delete-dish/:id')
  async deleteDish(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const dish = await DishService.deleteDish(id);
      res.status(200).json(dish);
    } catch (error: any) {
      res.status(500).json({ error: error.message });
    }
  }
}

export default new DishController();
