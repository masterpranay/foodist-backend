import { Request, Response } from "express";
import RestaurantService from "../services/restaurant.service";

class RestaurantController {
  // @Get('/get-all-restaurants-id')
  async getAllRestaurantsId(req: Request, res: Response) {
    let name = null
    if(req.query.name) {
      name = req.query.name
    }
    try {
      const filter = name ? `name~'${name}'` : ''
      const restaurants = await RestaurantService.getAllRestaurantsId(filter);
      res.status(200).json(restaurants);
    } catch (error: any) {
      res.status(500).json({ error: error.message });
    }
  }

  // @Get('/get-restaurant-by-id/:id')
  async getRestaurantById(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const restaurant = await RestaurantService.getRestaurantById(id);
      res.status(200).json(restaurant);
    } catch (error: any) {
      res.status(500).json({ error: error.message });
    }
  }

  // @Post('/create-restaurant')
  async createRestaurant(req: Request, res: Response) {
    try {
      const { body } = req;
      const restaurant = await RestaurantService.createRestaurant(body);
      res.status(200).json(restaurant);
    } catch (error: any) {
      res.status(500).json({ error: error.message });
    }
  }

  // @Patch('/update-restaurant/:id')
  async updateRestaurant(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const { body } = req;
      const restaurant = await RestaurantService.updateRestaurant(id, body);
      res.status(200).json(restaurant);
    } catch (error: any) {
      res.status(500).json({ error: error.message });
    }
  }

  // @Delete('/delete-restaurant/:id')
  async deleteRestaurant(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const result = await RestaurantService.deleteRestaurant(id);
      res.status(200).json(result);
    } catch (error: any) {
      res.status(500).json({ error: error.message });
    }
  }

  // @Get('/get-restaurant-by-userId/:userId')
  async getRestaurantByUserId(req: Request, res: Response) {
    try {
      const { userId } = req.params;
      const restaurant = await RestaurantService.getRestaurantByUserId(userId);
      res.status(200).json(restaurant);
    } catch (error: any) {
      res.status(500).json({ error: error.message });
    }
  }
}

export default new RestaurantController();
