import { Request, Response } from "express";
import UserService from "../services/user.service";

class UserController {
  async loginUser(req: Request, res: Response) {
    const { mobileNumber } = req.body;

    try {
      let user = await UserService.findUserByMobileNumber(mobileNumber);
      if (!user) {
        user = await UserService.createUser(mobileNumber);
      }
      return res.status(200).json(user);
    } catch (error: any) {
      return res.status(error.code).send(error.message);
    }
  }

  async sendOtp(req: Request, res: Response) {
    const { mobileNumber } = req.body;
    try {
      let user = await UserService.findUserByMobileNumber(mobileNumber);
      if (!user) {
        user = await UserService.createUser(mobileNumber);
      }
      // const otp = Math.floor(100000 + Math.random() * 900000);
      await UserService.updateOtp(user.id, 1234);
      return res.status(200).json({
        message: "OTP sent successfully",
      });
    } catch (error: any) {
      res.status(error.code).send(error.message);
    }
  }

  async verifyOtp(req: Request, res: Response) {
    const { mobileNumber, otp } = req.body;
    try {
      let user = await UserService.findUserByMobileNumber(mobileNumber);
      if (user.otp != otp) {
        return res.status(401).json({
          message: "Invalid OTP",
        });
      }
      return res.status(200).json({
        message: "OTP verified successfully",
      });
    } catch (error: any) {
      res.status(error.code).send(error.message);
    }
  }

  async getUserById(req: Request, res: Response) {
    const { id } = req.params;
    try {
      const user = await UserService.findUserById(id);
      return res.status(200).json(user);
    } catch (error: any) {
      res.status(error.code).send(error.message);
    }
  }

  async getAllUsersId(req: Request, res: Response) {
    try {
      const users = await UserService.getAllUsersId();
      return res.status(200).json(users);
    } catch (error: any) {
      res.status(error.code).send(error.message);
    }
  }

  async getAllUsersAndOwnersId(req: Request, res: Response) {
    try {
      const users = await UserService.getAllUsersAndOwnersId();
      return res.status(200).json(users);
    } catch (error: any) {
      res.status(error.code).send(error.message);
    }
  }

  async updateUserById(req: Request, res: Response) {
    const { id } = req.params;
    try {
      const user = await UserService.findUserById(id);
      if (!user) {
        return res.status(404).json({
          message: "User not found",
        });
      }
      await UserService.updateUser(id, {
        role: req.body.role,
        name: req.body.name,
        email: req.body.email,
      });
      return res.status(200).json({
        message: "User updated successfully",
      });
    } catch (error: any) {
      res.status(error.code).send(error.message);
    }
  }
}

export default new UserController();
