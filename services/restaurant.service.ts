class RestaurantService {
  async getAllRestaurantsId(filter: any) {
    console.log("filter", filter);
    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/restaurants/records?fields=id${filter ? `&filter=(${filter})` : ""}`
      );
      const resultJson = await result.json();
      const restaurants = resultJson.items;
      return restaurants;
    } catch (error: any) {
      throw error;
    }
  }

  async getRestaurantById(id: string) {
    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/restaurants/records?filter=(id='${id}')`
      );
      const resultJson = await result.json();
      const restaurant = resultJson.items[0];
      return restaurant;
    } catch (error: any) {
      throw error;
    }
  }

  async createRestaurant(body: any) {
    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/restaurants/records`,
        {
          method: "POST",
          body: JSON.stringify({
            name: body.name,
            address: body.address,
            image: body.image,
            rating: body.rating,
            categories: body.categories,
            city: body.city,
            state: body.state,
            pincode: body.pincode,
            userId: body.userId,
          }),
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      const restaurant = await result.json();
      return restaurant;
    } catch (error: any) {
      throw error;
    }
  }

  async updateRestaurant(id: string, body: any) {
    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/restaurants/records/${id}`,
        {
          method: "PATCH",
          body: JSON.stringify({
            name: body.name,
            address: body.address,
            image: body.image,
            rating: body.rating,
            categories: body.categories,
            city: body.city,
            state: body.state,
            pincode: body.pincode,
            userId: body.userId,
          }),
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      const restaurant = await result.json();
      return restaurant;
    } catch (error: any) {
      throw error;
    }
  }

  async deleteRestaurant(id: string) {
    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/restaurants/records/${id}`,
        {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      return {
        message: `Restaurant with id ${id} deleted successfully`,
      };
    } catch (error: any) {
      throw error;
    }
  }

  async getRestaurantByUserId(userId: string) {
    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/restaurants/records?filter=(userId='${userId}')`
      );
      const resultJson = await result.json();
      const restaurant = resultJson.items[0];
      return restaurant;
    } catch (error: any) {
      throw error;
    }
  }
}

export default new RestaurantService();
