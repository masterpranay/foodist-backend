interface Location {
  stateId: string;
  cityId: string;
  houseNumber: string;
  locality: string;
  pincode: string;
  isCurrentLocation: boolean;
  userId: string;
}

class LocationService {
  async getAllStates() {
    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/state/records`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      const data = await result.json();
      return data?.items;
    } catch (err) {
      throw err;
    }
  }

  async getAllCities(stateId: string) {
    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/city/records?filter=(stateId='${stateId}')`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      const data = await result.json();
      return data?.items;
    } catch (err) {
      throw err;
    }
  }

  async addLocation(location: Location) {
    try {
      console.log(location);
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/user_location/records`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(location),
        }
      );

      const data = await result.json();
      return data;
    } catch (err) {
      throw err;
    }
  }

  async updateLocation(location: Location, locationId: string) {
    try {
      console.log(location, locationId);
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/user_location/records/${locationId}`,
        {
          method: "PATCH",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(location),
        }
      );

      const data = await result.json();
      return data;
    } catch (err) {
      throw err;
    }
  }

  async deleteLocation(locationId: string) {
    try {
      await fetch(
        `${process.env.PB_URL}/api/collections/user_location/records/${locationId}`,
        {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      return true;
    } catch (err) {
      throw err;
    }
  }

  async getAllLocations(userId: string) {
    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/user_location/records?filter=(userId='${userId}')`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      const data = await result.json();
      return data?.items;
    } catch (err) {
      throw err;
    }
  }
}

export default new LocationService();
