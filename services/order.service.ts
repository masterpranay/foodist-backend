class OrderService {
  async createOrder(info: any) {
    const orderData = {
      dish: info.dish,
      restaurant: info.restaurant,
      user: info.user,
      itemQuantity: info.itemQuantity,
      totalPrice: info.totalPrice,
      status: "pending",
    };

    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/orders/records`,
        {
          method: "POST",
          body: JSON.stringify(orderData),
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      const data = await result.json();
      return data;
    } catch (err) {
      throw err;
    }
  }

  async getOrdersId(filter: any, sort?: any) {
    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/orders/records?filter=(${filter})&fields=id`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      const data = await result.json();
      return data?.items;
    } catch (err) {
      throw err;
    }
  }

  async getOrderById(id: string) {
    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/orders/records/${id}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      const data = await result.json();
      return data;
    } catch (err) {
      throw err;
    }
  }

  async getLastOrders({ page, limit }: { page: number; limit: number }) {
    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/orders/records?sort=created&page=${page}&perPage=${limit}&fields=id`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      const data = await result.json();
      return data?.items;
    } catch (err) {
      throw err;
    }
  }

  async updateOrder({ id, data }: { id: string; data: any }) {
    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/orders/records/${id}`,
        {
          method: "PATCH",
          body: JSON.stringify(data),
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      const updatedOrder = await result.json();
      return updatedOrder;
    } catch (err) {
      throw err;
    }
  }
}

export default new OrderService();
