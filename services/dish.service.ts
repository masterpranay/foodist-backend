class DishService {
  async getAllDishesId(restaurantId: string, filter: string) {
    try {
      const filterUpdated = `${filter && `${filter}%26%26`}restaurant%3D%27${restaurantId}%27`
      console.log(restaurantId, filterUpdated);
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/dishes/records?fields=id&filter=(${filterUpdated})`
      );
      const resultJson = await result.json();
      const dishes = resultJson.items;
      return dishes;
    } catch (error: any) {
      throw error;
    }
  }

  async getDishById(id: string) {
    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/dishes/records?filter=(id='${id}')`
      );
      const resultJson = await result.json();
      const dish = resultJson.items[0];
      return dish;
    } catch (error: any) {
      throw error;
    }
  }

  async createDish(body: any) {
    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/dishes/records`,
        {
          method: "POST",
          body: JSON.stringify({
            name: body.name,
            price: body.price,
            image: body.image,
            restaurant: body.restaurant,
            type: body.type,
            description: body.description,
            category: body.category,
          }),
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      const dish = await result.json();
      return dish;
    } catch (error: any) {
      throw error;
    }
  }

  async updateDish(id: string, body: any) {
    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/dishes/records/${id}`,
        {
          method: "PATCH",
          body: JSON.stringify({
            name: body.name,
            price: body.price,
            image: body.image,
            restaurant: body.restaurant,
            type: body.type,
            description: body.description,
            category: body.category,
          }),
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      const dish = await result.json();
      return dish;
    } catch (error: any) {
      throw error;
    }
  }

  async deleteDish(id: string) {
    try {
      const result = await fetch(
        `${process.env.PB_URL}/api/collections/dishes/records/${id}`,
        {
          method: "DELETE",
        }
      );
      return {
        message: "Dish deleted successfully",
      };
    } catch (error: any) {
      throw error;
    }
  }
}

export default new DishService();
