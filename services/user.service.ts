class UserService {
  async findUserByMobileNumber(mobileNumber: string) {
    try {
      const resultFetched = await fetch(
        `${process.env.PB_URL}/api/collections/users/records?filter=(mobileNumber='${mobileNumber}')`
      );
      const result = await resultFetched.json();
      const user = result.items[0];
      return user;
    } catch (error: any) {
      console.log(error);
      return error
    }
  }

  async createUser(mobileNumber: string) {
    try {
      const resultFetched = await fetch(
        `${process.env.PB_URL}/api/collections/users/records`,
        {
          method: "POST",
          body: JSON.stringify({ mobileNumber, role: "user" }),
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      const result = await resultFetched.json();
      return result;
    } catch (error: any) {
      console.log(error);
      return error
    }
  }

  async updateOtp(id: string, otp: number) {
    try {
      const resultFetched = await fetch(
        `${process.env.PB_URL}/api/collections/users/records/${id}`,
        {
          method: "PATCH",
          body: JSON.stringify({ otp }),
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      const result = await resultFetched.json();
      return result;
    } catch (error: any) {
      console.log(error);
      return error;
    }
  }

  async findUserById(id: string) {
    try {
      const resultFetched = await fetch(
        `${process.env.PB_URL}/api/collections/users/records/${id}`
      );
      const result = await resultFetched.json();
      const user = result;
      return user;
    } catch (error: any) {
      console.log(error);
      return error;
    }
  }

  async getAllUsersId() {
    try {
      const resultFetched = await fetch(
        `${process.env.PB_URL}/api/collections/users/records?fields=id&filter=(role='user')`
      );
      const result = await resultFetched.json();
      const users = result.items.map((item: any) => item.id);
      return users;
    } catch (error: any) {
      console.log(error);
      return error;
    }
  }

  async updateUser(id: string, user: any) {
    try {
      const resultFetched = await fetch(
        `${process.env.PB_URL}/api/collections/users/records/${id}`,
        {
          method: "PATCH",
          body: JSON.stringify(user),
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      const result = await resultFetched.json();
      return result;
    } catch (error: any) {
      console.log(error);
      return error;
    }
  }

  async getAllUsersAndOwnersId() {
    try {
      const resultFetched = await fetch(
        `${process.env.PB_URL}/api/collections/users/records?fields=id&filter=(role='user' || role='owner')`
      );
      const result = await resultFetched.json();
      const users = result.items.map((item: any) => item.id);
      return users;
    } catch (error: any) {
      console.log(error);
      return error;
    }
  }
}

export default new UserService();
