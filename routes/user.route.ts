import { Router } from "express";
import UserController from "../controllers/user.controller";

const router = Router();

// create an user and save mobile number
router.post("/login-user", UserController.loginUser);

// send otp to mobile number
router.post("/send-otp", UserController.sendOtp);

// verify otp
router.post("/verify-otp", UserController.verifyOtp);

// get user by id
router.get("/get-user-by-id/:id", UserController.getUserById);

router.get("/get-all-users-id", UserController.getAllUsersId);

// get all users and owners
router.get("/get-all-users-and-owners-id", UserController.getAllUsersAndOwnersId);

// update user by id
router.patch("/update-user-by-id/:id", UserController.updateUserById);

export default router;
