import express, { Request, Response } from 'express';
import restaurantRoute from './restaurant.route';
import dishRoute from './dish.route';
import userRoute from './user.route';
import orderRoute from './order.route';
import locationRoute from './location.route';

const router = express.Router();

router.get('/api/test', (req: Request, res: Response) => {
  res.send('API Test Route!! 🤓');
});

router.use('/api/restaurants', restaurantRoute);
router.use('/api/dishes', dishRoute);
router.use('/api/users', userRoute);
router.use('/api/orders', orderRoute);
router.use('/api/location', locationRoute);

export default router;
