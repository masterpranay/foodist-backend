import { Router } from "express";
import LocationController from "../controllers/location.controller";

const router = Router();

router.get("/get-all-states", LocationController.getAllStates);

router.get("/get-all-cities/:stateId", LocationController.getAllCities);

router.post("/add-location", LocationController.addLocation);

router.put("/update-location/:locationId", LocationController.updateLocation);

router.delete("/delete-location/:locationId", LocationController.deleteLocation);

router.get("/get-all-locations/:userId", LocationController.getAllLocations);

export default router;
