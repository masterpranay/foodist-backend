import { Router, Request, Response } from "express";
import RestaurantController from "../controllers/restaurant.controller";

const router = Router();

router.get("/get-all-restaurants-id", RestaurantController.getAllRestaurantsId);

router.get("/get-restaurant-by-id/:id", RestaurantController.getRestaurantById);

router.get("/get-restaurant-by-userId/:userId", RestaurantController.getRestaurantByUserId);

router.post("/create-restaurant", RestaurantController.createRestaurant);

router.patch("/update-restaurant/:id", RestaurantController.updateRestaurant);

router.delete("/delete-restaurant/:id", RestaurantController.deleteRestaurant);

export default router;
