import { Router } from "express";
import OrderController from "../controllers/order.controller";

const router = Router();

router.post("/create-order", OrderController.createOrder);

router.get("/get-orders-user/:userId", OrderController.getOrdersIdOfUser);

router.get(
  "/get-orders-restaurant/:restaurantId",
  OrderController.getOrdersIdOfRestaurant
);

router.get("/get-order/:orderId", OrderController.getOrderById);

// get last 10 orders id
router.get("/get-last-orders", OrderController.getLastOrders);

// accept the order
router.post("/accept-order/:orderId", OrderController.acceptOrder);

// reject the order
router.post("/reject-order/:orderId", OrderController.rejectOrder);

// dispatch the order
router.post("/dispatch-order/:orderId", OrderController.dispatchOrder);

// deliver the order
router.post("/deliver-order/:orderId", OrderController.deliverOrder);

export default router;
