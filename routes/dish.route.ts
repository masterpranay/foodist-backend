import { Router, Request, Response } from "express";
import DishController from "../controllers/dish.controller";

const router = Router();

router.get("/get-all-dishes-id/:restaurantId", DishController.getAllDishesId);

router.get("/get-dish-by-id/:id", DishController.getDishById);

router.post("/create-dish", DishController.createDish);

router.patch("/update-dish/:id", DishController.updateDish);

router.delete("/delete-dish/:id", DishController.deleteDish);

export default router;
