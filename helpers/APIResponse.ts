import { Response } from "express";

const successResponse = (res : Response, msg = "operation successful") => {
  const data = {
    status: 1,
    msg,
  };
  return res.status(200).json(data);
};

const successResponseWithData = (res : Response, data : any, msg = "operation successful") => {
  const resData = {
    status: 1,
    msg,
    data,
  };
  return res.status(200).json(resData);
};

const errorResponse = (res : Response, msg = "internal server error") => {
  const data = {
    status: 0,
    msg,
  };
  return res.status(500).json(data);
};

const notFoundResponse = (res : Response, msg = "resource not found") => {
  const data = {
    status: 0,
    msg,
  };
  return res.status(404).json(data);
};

const validationError = (res : Response, msg = "invalid data") => {
  const resData = {
    status: 0,
    msg,
  };
  return res.status(400).json(resData);
};

const validationErrorWithData = (res : Response, data : any, msg = "invalid data") => {
  const resData = {
    status: 0,
    msg,
    data,
  };
  return res.status(400).json(resData);
};

const unauthorizedResponse = (res : Response, msg = "unauthorized request") => {
  const data = {
    status: 0,
    msg,
  };
  return res.status(401).json(data);
};

export default {
  successResponse,
  successResponseWithData,
  errorResponse,
  notFoundResponse,
  validationErrorWithData,
  unauthorizedResponse,
  validationError,
};
